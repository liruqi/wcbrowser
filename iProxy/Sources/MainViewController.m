/*
 * Copyright 2010, Torsten Curdt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "MainViewController.h"
#import "InfoViewController.h"
#import "HTTPServer.h"
#import "PacFileResponse.h"
#import "SocksProxyServer.h"
#if HTTP_PROXY_ENABLED
#import "HTTPProxyServer.h"
#endif
#import "UIViewAdditions.h"
#import "UIColorAdditions.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <QuartzCore/QuartzCore.h>
#import "UIDevice_Extended.h"
#import "ProxyHTTPServer.h"
#import "ContentParser.h"
#import "ProxyHTTPServer.h"

#define OFTEN_UPDATE_PERIOD             0.5
#define NOT_SO_OFTEN_UPDATE_PERIOD      2.0

// defaults keys
#define KEY_SOCKS_ON    @"socks.on"
#define KEY_HTTP_ON     @"http.on"
extern int fdEventNum;
@interface MainViewController()
- (void)updateHTTPProxy;
- (void)updateSocksProxy;
- (void)updateTransfer;
@end

@implementation MainViewController

 


- (void)viewDidLoad
{

    [[ProxyHTTPServer sharedProxyHTTPServer] start];
    
    
    _applicationActive = YES;
    _windowVisible = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowDidBecomeVisible:) name:UIWindowDidBecomeVisibleNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowDidBecomeHidden:) name:UIWindowDidBecomeHiddenNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewWillAppear:(BOOL)animated
{
	
     [webView sizeToFit];
    
   
    [[SocksProxyServer sharedServer] addObserver:self forKeyPath:@"connectionCount" options:NSKeyValueObservingOptionNew context:nil];
	
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scheduleSocksProxyInfoTimer) name:HTTPProxyServerNewBandwidthStatNotification object:nil];
    [self updateHTTPProxy];
    [self updateSocksProxy];
}

- (void)viewDidAppear:(BOOL)animated
{
    _viewVisible = YES;
    if (_applicationActive && _windowVisible && !_updateTransferTimer) {
        [self updateTransfer];
    }
    //    NSLog(@"%@ application: %@ window: %@ view: %@", NSStringFromSelector(_cmd), _applicationActive?@"active":@"not active", _windowVisible?@"visible":@"hidden", _viewVisible?@"visible":@"hidden");
}

- (void)viewDidDisappear:(BOOL)animated
{
    _viewVisible = NO;
    [_updateTransferTimer invalidate];
    _updateTransferTimer = nil;
    [socksProxyInfoTimer invalidate];
    socksProxyInfoTimer = nil;
    [labelTimer invalidate];
    labelTimer = nil;
    
    //    NSLog(@"%@ application: %@ window: %@ view: %@", NSStringFromSelector(_cmd), _applicationActive?@"active":@"not active", _windowVisible?@"visible":@"hidden", _viewVisible?@"visible":@"hidden");
}


- (void)updateTransfer
{
    double uploadBandwidth;
    double downloadBandwidth;
	UInt64 oldIn = 0;
	UInt64 oldOut = 0;
	
    _updateTransferTimer = nil;
    [[SocksProxyServer sharedServer] getBandwidthStatWithUpload:&uploadBandwidth download:&downloadBandwidth];
    [[SocksProxyServer sharedServer] getTotalBytesWithUpload:&oldOut download:&oldIn];
 
 
    if (_applicationActive && _windowVisible && _viewVisible) {
        if ((uploadBandwidth != 0) || (downloadBandwidth != 0)) {
            _updateTransferTimer = [NSTimer scheduledTimerWithTimeInterval:OFTEN_UPDATE_PERIOD target:self selector:@selector(updateTransfer) userInfo:nil repeats:NO];
        } else {
            _updateTransferTimer = [NSTimer scheduledTimerWithTimeInterval:NOT_SO_OFTEN_UPDATE_PERIOD target:self selector:@selector(updateTransfer) userInfo:nil repeats:NO];	
        }
    }
//    NSLog(@"application: %@ window: %@ view: %@", _applicationActive?@"active":@"not active", _windowVisible?@"visible":@"hidden", _viewVisible?@"visible":@"hidden");
}



- (void)applicationWillResignActive:(NSNotification *)notification
{
    if (_applicationActive) {
        _applicationActive = NO;
        [_updateTransferTimer invalidate];
        _updateTransferTimer = nil;
    }
//    NSLog(@"%@ application: %@ window: %@ view: %@", NSStringFromSelector(_cmd), _applicationActive?@"active":@"not active", _windowVisible?@"visible":@"hidden", _viewVisible?@"visible":@"hidden");
}

- (void)applicationDidBecomeActive:(NSNotification *)notification
{
    if (!_applicationActive) {
        _applicationActive = YES;
        if (_windowVisible && _viewVisible && !_updateTransferTimer) {
            [self updateTransfer];
        }
    }
//    NSLog(@"%@ application: %@ window: %@ view: %@", NSStringFromSelector(_cmd), _applicationActive?@"active":@"not active", _windowVisible?@"visible":@"hidden", _viewVisible?@"visible":@"hidden");
}

- (void)windowDidBecomeVisible:(NSNotification *)notification
{
    if (!_windowVisible) {
        _windowVisible = YES;
        if (_applicationActive && _viewVisible && !_updateTransferTimer) {
            [self updateTransfer];
        }
    }
//    NSLog(@"%@ application: %@ window: %@ view: %@", NSStringFromSelector(_cmd), _applicationActive?@"active":@"not active", _windowVisible?@"visible":@"hidden", _viewVisible?@"visible":@"hidden");
}

- (void)windowDidBecomeHidden:(NSNotification *)notification
{
    if (_windowVisible) {
        _windowVisible = NO;
        [_updateTransferTimer invalidate];
        _updateTransferTimer = nil;
    }
//    NSLog(@"%@ application: %@ window: %@ view: %@", NSStringFromSelector(_cmd), _applicationActive?@"active":@"not active", _windowVisible?@"visible":@"hidden", _viewVisible?@"visible":@"hidden");
}
 


- (void)scheduleSocksProxyInfoTimer
{
    if (!socksProxyInfoTimer) {
        socksProxyInfoTimer = [NSTimer scheduledTimerWithTimeInterval:OFTEN_UPDATE_PERIOD target:self selector:@selector(updateSocksProxyInfo) userInfo:nil repeats:NO];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (object == [SocksProxyServer sharedServer] && [keyPath isEqualToString:@"connectionCount"]) {
    	[self scheduleSocksProxyInfoTimer];
    }
}

- (void)updateSocksProxyInfo
{
 
    socksProxyInfoTimer = nil;
}

- (void)updateHTTPProxy
{
    [(GenericServer *)[HTTPProxyServer sharedServer] start];
 
}

- (void)updateSocksProxy
{

    //[(GenericServer *)[SocksProxyServer sharedServer] start];

 

 
}

 

- (IBAction)resetTransfer:(id)sender
{
    [[SocksProxyServer sharedServer] resetTotalBytes];
}

#pragma mark socks proxy

- (void) httpURLAction:(id)sender
{
 
}

- (void) socksURLAction:(id)sender
{
  
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch (buttonIndex) {
        case 0:
        	{
                MFMailComposeViewController*	messageController = [[MFMailComposeViewController alloc] init];
                
                if ([messageController respondsToSelector:@selector(setModalPresentationStyle:)])	// XXX not available in 3.1.3
                    messageController.modalPresentationStyle = UIModalPresentationFormSheet;
                    
                messageController.mailComposeDelegate = self;
                [messageController setMessageBody:emailBody isHTML:NO];
                [self presentModalViewController:messageController animated:YES];
                [messageController release];
            }
            break;
        case 1:
        	{
				NSDictionary *items;
                
				items = [NSDictionary dictionaryWithObjectsAndKeys:emailURL, kUTTypePlainText, emailURL, kUTTypeText, emailURL, kUTTypeUTF8PlainText, [NSURL URLWithString:emailURL], kUTTypeURL, nil];
                [UIPasteboard generalPasteboard].items = [NSArray arrayWithObjects:items, nil];
            }
        	break;
        default:
            break;
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{
	[self dismissModalViewControllerAnimated:YES];
}










- (IBAction)openURL:(id)sender
{
	[urlBar resignFirstResponder];
	NSURL *url = [NSURL URLWithString:[urlBar text]];
	if (![url scheme]) {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[urlBar text]]];
	}
	if (!url) {
		return;
	}
	[self loadURL:url];
}

- (void)loadURL:(NSURL *)url
{
	NSURL *newURL = [NSURL URLWithString:[ContentParser localURLForURL:[url absoluteString] withBaseURL:nil]];
    [webView loadHTMLString:@"<html><body>dddddddd</body></html>" baseURL:nil];
    //webView.backgroundColor = [UIColor redColor];
    	[webView loadRequest:[NSURLRequest requestWithURL:newURL]];
}

- (void)webViewDidFinishLoad:(UIWebView *)wv
{
	NSArray *parts = [[[[wv request] URL] absoluteString] componentsSeparatedByString:@"?url="];
	if ([parts count] > 1) {
		NSString *realURL = (NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)[parts objectAtIndex:1],(CFStringRef)@"",kCFStringEncodingUTF8);
		[urlBar setText:realURL];
	}
}

// We'll take over the page load when the user clicks on a link
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)theRequest navigationType:(UIWebViewNavigationType)navigationType
{
	if (![[[theRequest URL] scheme] isEqualToString:@"http"] && ![[[theRequest URL] scheme] isEqualToString:@"https"]) {
		return YES;
	}
    
    
    //
    NSMutableURLRequest *request = (NSMutableURLRequest *)theRequest;
    
    if ([request respondsToSelector:@selector(setValue:forHTTPHeaderField:)]) {
        [request setValue:[NSString stringWithFormat:@"%@ Safari/528.16", [request valueForHTTPHeaderField:@"User-Agent"]] forHTTPHeaderField:@"User_Agent"];
    }
    
    
	if (navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeFormSubmitted || navigationType == UIWebViewNavigationTypeFormResubmitted) {
		if ([[[[theRequest URL] absoluteURL] host] isEqualToString:@"127.0.0.1"]) {
			return YES;
		}
		[urlBar setText:[[theRequest URL] absoluteString]];
		[self loadURL:[theRequest URL]];		
		return NO;
	}
	
	// Other request types are often things like iframe content, we have no choice but to let UIWebView load them itself
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[self openURL:nil];
	return NO;
}


- (IBAction)historyBack:(id)sender
{
	[webView goBack];
}
- (IBAction)historyForward:(id)sender
{
	[webView goForward];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}



@end
