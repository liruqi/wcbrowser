//
//  ProxyHTTPServer.h
//  TextTransfer
//
//  Created by Matt Gallagher on 2009/07/13.
//  Copyright 2009 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//


#import <UIKit/UIKit.h>

 

typedef enum
{
	PROXY_SERVER_STATE_IDLE,
	PROXY_SERVER_STATE_STARTING,
	PROXY_SERVER_STATE_RUNNING,
	PROXY_SERVER_STATE_STOPPING
} ProxyHTTPServerState;

@class ProxyHTTPResponseHandler;

@interface ProxyHTTPServer : NSObject
{
	NSError *lastError;
	NSFileHandle *listeningHandle;
	CFSocketRef socket;
	ProxyHTTPServerState state;
	CFMutableDictionaryRef incomingRequests;
	NSMutableSet *responseHandlers;
}

@property (nonatomic, readonly, retain) NSError *lastError;
@property (readonly, assign) ProxyHTTPServerState state;

+ (ProxyHTTPServer *)sharedProxyHTTPServer;

- (void)start;
- (void)stop;

- (void)closeHandler:(ProxyHTTPResponseHandler *)aHandler;

@end

extern NSString * const ProxyHTTPServerNotificationStateChanged;
